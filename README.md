
### Handbook

https://docs.google.com/document/d/17RZ1eDKmHssJw94JGmKiqAfAdu47o9lcr5BrPXsZyJs/edit?usp=sharing

### Installation

`nodejs-facebook-api` requires [Node.js](https://nodejs.org/) v6+ to run.

On Ubuntu >=14.04:

**Node,js**

```sh
$ sudo apt-get update
$ sudo apt-get install build-essential libssl-dev
$ curl -sL https://deb.nodesource.com/setup_6.x -o nodesource_setup.sh
$ sudo bash nodesource_setup.sh
$ sudo apt-get install nodejs
$ sudo apt-get install npm
```
**MongoDB**

```sh
$ sudo apt-get update
$ sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
$ echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
$ sudo apt-get update
$ sudo apt-get install -y mongodb-org
$ sudo nano /etc/systemd/system/mongodb.service
```
Add this:

```sh
[Unit]
Description=High-performance, schema-free document-oriented database
After=network.target

[Service]
User=mongodb
ExecStart=/usr/bin/mongod --quiet --config /etc/mongod.conf

[Install]
WantedBy=multi-user.target
```

```sh
$ sudo systemctl start mongodb
$ sudo systemctl enable mongodb
```

if have a problem, try:

```sh
sudo apt-get install --reinstall mongodb
```

To preparing the environment.

```sh
$ cd /var/www/html
$ git clone https://gitlab.com/williamnvk/yoohood-api-nodejs
$ cd yoohood-api-nodejs
$ npm install
```
