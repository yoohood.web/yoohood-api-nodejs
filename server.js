
    'use strict'

    var compression = require('compression')


    var express     = require('express'),
    app             = express(),
    server          = "127.0.0.1",
    port            = 8081,
    mongoose        = require('mongoose'),
    userModel       = require('./src/application/models/userModel'),
    checkInModel    = require('./src/application/models/checkInModel'),
    eventModel      = require('./src/application/models/eventModel'),
    bodyParser      = require('body-parser'),
    errorHandler    = require('errorhandler'),
    methodOverride  = require('method-override'),
    logger          = require('morgan'),
    router          = express.Router(),
    cors            = require('cors')


    app.use(cors())
    app.use(compression())

    app.use(bodyParser.json())
    app.use(bodyParser.urlencoded({
        extended: false
    }))

    app.use(methodOverride('X-HTTP-Method-Override'))

    mongoose.Promise = global.Promise
    mongoose.connect('mongodb://localhost:27017/facebook',  { useMongoClient: true })

    app.set('port', process.env.PORT || port)
    app.set('view engine', 'html')

    app.enable('strict routing')

    app.use(logger('combined'))



    app.use(errorHandler({
        dumpExceptions: true,
        showStack: true
    }))

    router.get("/api/",function(req,res){
        res.json({"status" : true, "message" : "YooHood Api"})
    })

    var routes = require('./src/application/routes')
    routes(app)
    app.use('/', router)

    app.use(function(req, res, next) {
        res.status(404).send({url: req.originalUrl + ' not found'})
        res.setHeader('Access-Control-Allow-Origin', '*')
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE')
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type')
        res.setHeader('Access-Control-Allow-Credentials', true)
        next()
    })

    app.listen(port)

    console.log('YooHood RESTful API server started and working on: ' + server + ':' + port)
