var mongoose    = require('mongoose')
var mongoSchema = mongoose.Schema

var checkInSchema  = new mongoSchema({
    created_at  : {
        type    : Date,
        default : Date.now
    },
    event  : {
        type: [
            {
                type    : mongoSchema.ObjectId,
                ref     : "eventModel"
            }
        ]
    },
    user  : {
        type: [
            {
                type    : mongoSchema.ObjectId,
                ref     : "userModel"
            }
        ]
    }
})

module.exports = mongoose.model('checkInModel', checkInSchema)
