var mongoose    = require('mongoose')
var mongoSchema = mongoose.Schema

var userSchema  = new mongoSchema({
    name        : {
        type        : String,
        lowercase   : true,
        trim        : true
    },
    created_at  : {
        type    : Date,
        default : Date.now
    },
    email       : {
        type        : String,
        index       : true,
        lowercase   : true,
        trim        : true
    },
    last_login_at  : {
        type    : Date,
        default : Date.now
    },
    my_events  : {
        type: [
            {
                type    : mongoSchema.ObjectId,
                ref     : "eventModel"
            }
        ]
    },
    gid: {
        type: []
    },
    distance: {
        type: String,
        default: '5'
    }
})

module.exports = mongoose.model('userModel', userSchema)
