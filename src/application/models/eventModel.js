var mongoose    = require('mongoose')
var mongoSchema = mongoose.Schema

var eventSchema = new mongoSchema({
    facebook        : {
        type        : String,
        trim        : true,
        index       : true
    },
    name        : {
        type        : String,
        trim        : true,
        index       : true
    },
    created_at  : {
        type    : Date,
        default : Date.now
    },
    start_at  : {
        type    : Date,
        default : Date.now
    },
    end_at  : {
        type    : Date,
        default : Date.now
    },
    description : {
        type        : String,
        trim        : true
    },
    cover       : {
        type        : String,
        trim        : true
    },
    owner       : {
        type        : []
    },
    type        : {
        type        : String,
        trim        : true,
        index       : true
    },
    updated_at  : {
        type    : Date,
        default : Date.now
    },
    sponsored        : {
        type        : Boolean,
        default     : false
    },
    category        : {
        type        : String,
        trim        : true
    },
    ticket_uri        : {
        type        : String,
        trim        : true
    },
    city        : {
        type        : String,
        trim        : true
    },
    place        : {
        type        : String,
        trim        : true
    },
    location        : {
        type        : []
    }
})

module.exports = mongoose.model('eventModel', eventSchema)
