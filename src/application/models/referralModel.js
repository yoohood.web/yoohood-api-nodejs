var mongoose    = require('mongoose')
var mongoSchema = mongoose.Schema

var  referralSchema  = new mongoSchema({
    name        : {
        type        : String,
        lowercase   : true,
        trim        : true
    },
    created_at  : {
        type    : Date,
        default : Date.now
    },
    event  : {
        type: [
            {
                type    : mongoSchema.ObjectId,
                ref     : "eventModel"
            }
        ]
    },
    user  : {
        type: [
            {
                type    : mongoSchema.ObjectId,
                ref     : "userModel"
            }
        ]
    }
})

module.exports = mongoose.model('userModel',  referralSchema)
