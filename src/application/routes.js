'use strict'
module.exports      = function(app) {

    /*  user
        -------- */
    var userCtrl    = require('../application/controllers/userController')
    app.route('/api/user')
    .get(userCtrl.get)
    .post(userCtrl.insert)

    app.route('/api/user/:id')
    .get(userCtrl.one)
    .put(userCtrl.update)
    .delete(userCtrl.delete)

    app.route('/api/user/:id/distance')
    .put(userCtrl.updateDistance)

    app.route('/api/user/:id/event')
    .get(userCtrl.events)

    app.route('/api/user/:id/event/add')
    .put(userCtrl.addEvent)

    app.route('/api/user/:id/event/drop')
    .put(userCtrl.dropEvent)

    app.route('/api/user/:id/event/today')
    .get(userCtrl.today)

    app.route('/api/user/:id/event/week')
    .get(userCtrl.week)

    app.route('/api/user/:id/event/month')
    .get(userCtrl.month)


    app.route('/api/user/email/:email')
    .get(userCtrl.email)

    app.route('/api/user/facebook/:facebook')
    .get(userCtrl.facebook)

    app.route('/api/user/:id/isMy/:ev')
    .get(userCtrl.isMy)


    /*  event
        -------- */
    var eventCtrl   = require('../application/controllers/eventController')

    app.route('/api/event')
    .get(eventCtrl.get)
    .post(eventCtrl.insert)

    app.route('/api/event/:id')
    .get(eventCtrl.one)
    .delete(eventCtrl.delete)

    app.route('/api/event/update/:id')
    .put(eventCtrl.update)

    app.route('/api/event/facebook/:facebook')
    .get(eventCtrl.facebook)

    app.route('/api/event/sponsored/:limit')
    .get(eventCtrl.sponsored)

    app.route('/api/filter/events')
    .get(eventCtrl.filters)

    app.route('/api/event/search/category/:cat')
    .get(eventCtrl.searchByCategory)

    app.route('/api/event/search/city/:cit')
    .get(eventCtrl.searchByCity)


    /*  check-in
        -------- */
    var checkInCtrl   = require('../application/controllers/checkInController')

    app.route('/api/user/:id/check-in')
    .post(checkInCtrl.insert)
    app.route('/api/user/:id/:event/check-in')
    .get(checkInCtrl.check)


}
