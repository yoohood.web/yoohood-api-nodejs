'use strict'

var mongoose    = require('mongoose'),
    User        = mongoose.model('userModel'),
    ObjectID    = require("mongodb").ObjectID,
    eventModel  = mongoose.model('eventModel')


exports.one     = function(req, res) {
    User.findById(ObjectID(req.params.id), function(err, user){
        if (err) {
            res.json({"status" : false, "message" : "error fetching data", "data": err})
        } else {
            if (user instanceof Object) {
                res.json({"status" : true, "message" : "found", "data": user})
            } else {
                res.json({"status" : false, "message" : "empty"})
            }
        }
    })
}

exports.email     = function(req, res) {
    User.findOne({"email": req.params.email}, function(err, user){
        if (err) {
            res.json({"status" : false, "message" : "error fetching data", "data": err})
        } else {
            if (user instanceof Object) {
                res.json({"status" : true, "message" : "found", "data": user})
            } else {
                res.json({"status" : false, "message" : "empty"})
            }
        }
    })
}

exports.facebook     = function(req, res) {
    User.findOne({"gid": {
        "facebook": req.params.facebook}}, function(err, user){
        if (err) {
            res.json({"status" : false, "message" : "error fetching data", "data": err})
        } else {
            if (user instanceof Object) {
                res.json({"status" : true, "message" : "found", "data": user})
            } else {
                res.json({"status" : false, "message" : "empty"})
            }
        }
    })
}

exports.get     = function(req, res) {
    User.find({},function(err, user){
        if(err) {
            res.json({"status" : false, "message" : "error fetching data"})
        } else {
            if (user instanceof Object) {
                res.json({"status" : true, "message" : "found", "data": user})
            } else {
                res.json({"status" : false, "message" : "empty"})
            }
        }
    })
}


exports.insert = (req, res, next) => {
    User.findOne({"email": req.body.email}).exec()
    .then(
        user => {
            if (user) {
                res.json({"status" : false, "message" : "e-mail already in use", "data": user})
            } else {
                let n = new User(req.body)
                n.save()
                .then(
                    user => {
                        if (user) {
                            res.json({"status": true, "message": "created", "data": user})
                        } else {
                            res.json({"status": false, "message": "not created"})
                        }
                    },
                    err => {
                        res.status(400).json({ "status": false, "message": "an erros has ocurred", "data": err })
                    }
                )
            }
        },
        err => {
            res.status(400).json({ "status": false, "message": "an erros has ocurred", "data": err })
        }
    )
}

exports.delete      = function(req, res){
    User.remove({"_id": ObjectID(req.params.id)}, function(err, data){
        if(err) {
            res.json({"status": false, "message": "an error has occurred", "data": err})
        } else {
            res.json({"status": true, "message": "deleted"})
        }
    })
}

exports.update    = function(req, res){
    User.findById(ObjectID(req.params.id), function(err, user){
        if(err) {
            res.json({"status" : false, "message" : "error fetching data"})
        } else {
            if (user instanceof Object) {
                if(req.body.email !== undefined) {
                    user.email = req.body.email
                }
                if(req.body.facebook !== undefined) {
                    user.facebook = req.body.facebook
                }
                if(req.body.name !== undefined) {
                    user.name = req.body.name
                }
                user.save(function(err){
                    if(err) {
                        res.json({"status": false, "message": "an error has occurred", "data": err})
                    } else {
                        res.json({"status": true, "message": "updated"})
                    }
                })
            } else {
                res.json({"status": false, "message": "user not found, not updated"})
            }
        }
    })
}


exports.addEvent    = function(req, res){
    User.findById(ObjectID(req.params.id)).exec()
    .then(
        user => {
            if (user) {

                eventModel.findOne({"facebook": req.body.event}).exec()
                .then(
                    event => {
                        if (event) {
                            let id      = ObjectID(event._id)
                            let arr     = user.my_events
                            let check   = arr.indexOf(id) > -1
                            if ( check === true ) {
                                res.json({"status" : false, "message" : "user already linked to event", "data": user})
                            } else {
                                user.my_events.push(id)
                                user.save(function(err, user){
                                    res.json({"status" : true, "message" : "event linked", "data": user})
                                })
                            }

                        } else {
                            res.json({"status" : false, "message" : "event not found"})
                        }
                    },
                    err => {
                        res.json({"status" : false, "message" : "an error has ocurred on event", "data": err})
                    }
                )
            } else {
                res.json({"status" : false, "message" : "user not found"})
            }
        },
        err => {
            res.json({"status" : false, "message" : "error fetching data", "data": err})
        }
    )
}

exports.dropEvent    = function(req, res){
    User.findById(ObjectID(req.params.id)).exec()
    .then(
        user => {
            if (user) {

                eventModel.findOne({"facebook": req.body.event}).exec()
                .then(
                    event => {
                        if (event) {
                            let id      = ObjectID(event._id)
                            let arr     = user.my_events
                            let check   = arr.indexOf(id) > -1
                            if ( check === true ) {

                                user.my_events.pull(id)
                                user.save(function(err, user){
                                    res.json({"status" : true, "message" : "event dropped", "data": user})
                                })

                            } else {
                                user.my_events.push(id)
                                user.save(function(err, user){
                                    res.json({"status" : false, "message" : "event not linked", "data": user})
                                })
                            }

                        } else {
                            res.json({"status" : false, "message" : "event not found from drop event"})
                        }
                    },
                    err => {
                        res.json({"status" : false, "message" : "an error has ocurred on event", "data": err})
                    }
                )
            } else {
                res.json({"status" : false, "message" : "user not found"})
            }
        },
        err => {
            res.json({"status" : false, "message" : "error fetching data", "data": err})
        }
    )
}

exports.events    = function(req, res){
    User.findById(ObjectID(req.params.id), "my_events")
    .populate("my_events", "_id name cover start_at ent_at description facebook")
    .exec()
    .then(
        user => {
            if (user) {
                res.json({"status" : true, "message" : "user found", "data": user, "count": user.my_events.length})
            } else {
                res.json({"status" : false, "message" : "user not found"})
            }
        },
        err => {
            res.json({"status" : false, "message" : "error fetching data", "data": err})
        }
    )
}


exports.today     = function(req, res) {

    var today = new Date()
    var dd = today.getDate()
    var mm = today.getMonth()+1
    var yyyy = today.getFullYear()
    let date = yyyy + '-' + mm + '-' + dd

    User.findById(ObjectID(req.params.id), "my_events")
    .populate({
        path: 'my_events',
        select: '_id',
        match: {
            start_at: { $gte: date, $lte: date }
        }
    })
    .exec()
    .then(
        user => {
            if (user) {
                res.json({"status" : true, "message" : "user found", "data": user.my_events.length})
            } else {
                res.json({"status" : false, "message" : "user not found"})
            }
        },
        err => {
            res.json({"status" : false, "message" : "error fetching data", "data": err})
        }
    )
}


exports.week     = function(req, res) {

    let day_milliseconds = 24*60*60*1000;
    let current_date = new Date();
    let monday = new Date(current_date.getTime()-(current_date.getDay()-1)*day_milliseconds);
    let sunday = new Date(monday.getTime()+6*day_milliseconds);

    monday = monday.getFullYear() + '-' + monday.getMonth() + '-' +  monday.getDate()
    sunday = sunday.getFullYear() + '-' + sunday.getMonth() + '-' +  sunday.getDate()

    User.findById(ObjectID(req.params.id), "my_events")
    .populate({
        path: 'my_events',
        select: '_id',
        match: {
            start_at: { $gte: monday, $lte: sunday }
        }
    })
    .exec()
    .then(
        user => {
            if (user) {
                res.json({"status" : true, "message" : "user found", "data": user.my_events.length})
            } else {
                res.json({"status" : false, "message" : "user not found"})
            }
        },
        err => {
            res.json({"status" : false, "message" : "error fetching data", "data": err})
        }
    )
}


exports.month     = function(req, res) {

    let current_date = new Date();
    let date         = new Date(current_date.getFullYear(), current_date.getMonth(), 0)
    let last_day     = date.getDate()
    let start_date   = date.getFullYear() + '-' + date.getMonth() + '-' + '01'
    let end_date     = date.getFullYear() + '-' + date.getMonth() + '-' + last_day

    User.findById(ObjectID(req.params.id), "my_events")
    .populate({
        path: 'my_events',
        select: '_id',
        match: {
            start_at: { $gte: start_date, $lte: end_date }
        }
    })
    .exec()
    .then(
        user => {
            if (user) {
                res.json({"status" : true, "message" : "user found", "data": user.my_events.length})
            } else {
                res.json({"status" : false, "message" : "user not found"})
            }
        },
        err => {
            res.json({"status" : false, "message" : "error fetching data", "data": err})
        }
    )
}


exports.isMy     = function(req, res) {

    User.findById(ObjectID(req.params.id), "my_events")
    .populate({
        path: 'my_events',
        select: '_id',
        match: {
            _id: ObjectID(req.params.ev)
        }
    })
    .exec()
    .then(
        user => {
            if (user) {
                if (user.my_events.length > 0) {
                    res.json({"status" : true, "message" : "found"})
                } else {
                    res.json({"status" : false, "message" : "not found"})
                }
            } else {
                res.json({"status" : false, "message" : "user not found"})
            }
        },
        err => {
            res.json({"status" : false, "message" : "error fetching data", "data": err})
        }
    )
}

exports.updateDistance    = function(req, res){
    User.findById(ObjectID(req.params.id), function(err, event){
        if(err) {
            res.json({"status" : false, "message" : "error fetching data"})
        } else {

            let data = {}

            if(req.body.distance !== undefined) {
                data.distance = req.body.distance
            }

            User.update({_id: ObjectID(req.params.id)}, data, {upsert: true}, function(err){
                if(err) {
                    res.json({"status": false, "message": "an error has occurred", "data": err})
                } else {
                    res.json({"status": true, "message": "updated"})
                }
            })

        }
    })
}
