var mongoose        = require('mongoose'),
    ObjectID        = require("mongodb").ObjectID,
    eventModel      = mongoose.model('eventModel'),
    userModel       = mongoose.model('userModel'),
    checkInModel    = mongoose.model('checkInModel')

exports.insert    = function(req, res){
    userModel.findById(ObjectID(req.params.id)).exec()
    .then(
        user => {
            if (user) {

                eventModel.findById(ObjectID(req.body.event)).exec()
                .then(
                    event => {
                        if (event) {
                            let data = {"user": user._id, "event": event._id}
                            checkInModel.findOne(data).exec()
                            .then(
                                checkIn => {
                                    if (checkIn) {
                                        res.json({"status" : false, "message" : "already checked in"})
                                    } else {
                                        let checkIn = new checkInModel(data)
                                        checkIn.save().then(
                                            checkin => {
                                                res.json({"status" : true, "message" : "sucessfull"})
                                            },
                                            err => {
                                                res.json({"status" : false, "message" : "an error has ocurred on checkIn", "data": err})
                                            }
                                        )
                                    }
                                },
                                err => {
                                    res.json({"status" : false, "message" : "an error has ocurred on event", "data": err})
                                }
                            )
                        } else {
                            res.json({"status" : false, "message" : "event not found"})
                        }
                    },
                    err => {
                        res.json({"status" : false, "message" : "an error has ocurred on event", "data": err})
                    }
                )
            } else {
                res.json({"status" : false, "message" : "user not found"})
            }
        },
        err => {
            res.json({"status" : false, "message" : "an error has ocurred on user", "data": err})
        }
    )
}

exports.check     = function(req, res) {
    userModel.findById(ObjectID(req.params.id)).exec()
    .then(
        user => {
            if (user) {

                eventModel.findById(ObjectID(req.params.event)).exec()
                .then(
                    event => {
                        if (event) {
                            let data = {"user": user._id, "event": event._id}
                            checkInModel.findOne(data)
                            .populate("user", "name")
                            .populate("event", "name")
                            .exec()
                            .then(
                                checkIn => {
                                    if (checkIn) {
                                        res.json({"status" : true, "message" : "already checked in", "data": checkIn})
                                    } else {
                                        res.json({"status" : false, "message" : "not checked in"})
                                    }
                                },
                                err => {
                                    res.json({"status" : false, "message" : "an error has ocurred on event", "data": err})
                                }
                            )
                        } else {
                            res.json({"status" : false, "message" : "event not found"})
                        }
                    },
                    err => {
                        res.json({"status" : false, "message" : "an error has ocurred on event", "data": err})
                    }
                )
            } else {
                res.json({"status" : false, "message" : "user not found"})
            }
        },
        err => {
            res.json({"status" : false, "message" : "an error has ocurred on user", "data": err})
        }
    )
}
