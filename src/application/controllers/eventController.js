'use strict'

var mongoose    = require('mongoose'),
    Event        = mongoose.model('eventModel'),
    ObjectID    = require("mongodb").ObjectID

exports.one     = function(req, res) {
    Event.findById(ObjectID(req.params.id), function(err, event){
        if (err) {
            res.json({"status" : false, "message" : "error fetching data", "data": err})
        } else {
            if (event instanceof Object) {
                res.json({"status" : true, "message" : "found", "data": event})
            } else {
                res.json({"status" : false, "message" : "empty"})
            }
        }
    })
}



exports.facebook     = function(req, res) {
    Event.findOne({"facebook": req.params.facebook}, function(err, event){
        if (err) {
            res.json({"status" : false, "message" : "error fetching data", "data": err})
        } else {
            if (event instanceof Object) {
                res.json({"status" : true, "message" : "found", "data": event})
            } else {
                res.json({"status" : false, "message" : "empty"})
            }
        }
    })
}

exports.get     = function(req, res) {

    var query = Event.find({}).select("-description")

    query.exec(function (err, event){
        if(err) {
            res.json({"status" : false, "message" : "error fetching data"})
        } else {
            if (event instanceof Object) {
                res.json({"status" : true, "message" : "found", "data": event})
            } else {
                res.json({"status" : false, "message" : "empty"})
            }
        }
    })
}

exports.insert = (req, res, next) => {
    Event.findOne({"facebook": req.body.facebook}).exec()
    .then(
        event => {
            if (event) {
                res.json({"status" : false, "message" : "event already in database"})
            } else {
                let n = new Event(req.body)
                n.save()
                .then(
                    event => {
                        if (event) {
                            res.json({"status": true, "message": "created", "event": event})
                        } else {
                            res.json({"status": false, "message": "not created"})
                        }
                    },
                    err => {
                        res.json({ "status": false, "message": "an error has ocurred", "data": err })
                    }
                )
            }
        },
        err => {
            res.json({ "status": false, "message": "an error has ocurred", "data": err })
        }
    )
}

exports.delete      = function(req, res){
    Event.remove({"_id": ObjectID(req.params.id)}, function(err, data){
        if(err) {
            res.json({"status": false, "message": "an error has occurred", "data": err})
        } else {
            res.json({"status": true, "message": "deleted"})
        }
    })
}

exports.update    = function(req, res){
    Event.findById(ObjectID(req.params.id), function(err, event){
        if(err) {
            res.json({"status" : false, "message" : "error fetching data"})
        } else {

            let data = {}

            if(req.body.name !== undefined) {
                data.name = req.body.name
            }

            if(req.body.description !== undefined) {
                data.description = req.body.description
            }

            if(req.body.ticket_uri !== undefined) {
                data.ticket_uri = req.body.ticket_uri
            }

            if(req.body.category !== undefined) {
                data.category = req.body.category
            }

            if(req.body.city !== undefined) {
                data.city = req.body.city
            }

            if(req.body.sponsored !== undefined) {
                data.sponsored = req.body.sponsored
            }

            data.updated_at = Date.now()

            Event.update({_id: ObjectID(req.params.id)}, data, {upsert: true}, function(err){
                if(err) {
                    res.json({"status": false, "message": "an error has occurred", "data": err})
                } else {
                    res.json({"status": true, "message": "updated with test"})
                }
            })

        }
    })
}


exports.sponsored     = function(req, res) {

    let limit = parseInt(req.params.limit)

    Event.find(
        {
            sponsored: false
        }
    )
    .limit(limit)
    .sort({end_at: -1})
    .exec()
    .then(
        events => {
            if (events instanceof Object) {
                res.json({"status" : true, "message" : "found", "data": events})
            } else {
                res.json({"status" : false, "message" : "empty"})
            }
        }
    )
    .catch(
        err => {
            res.json({"status" : false, "message" : "err", "data": err})
        }
    )
}

exports.searchByCategory     = function(req, res) {

    Event.find(
        {
            category: req.params.cat
        }
    )
    .select("-description")
    .exec()
    .then(
        events => {
            if (events instanceof Object) {
                res.json({"status" : true, "message" : "found", "data": events})
            } else {
                res.json({"status" : false, "message" : "empty"})
            }
        }
    )
    .catch(
        err => {
            res.json({"status" : false, "message" : "err", "data": err})
        }
    )
}

exports.searchByCity     = function(req, res) {

    Event.find(
        {
            city: new RegExp('^' + req.params.cit + '$', "i")
        }
    )
    .select("-description")
    .exec()
    .then(
        events => {
            if (events instanceof Object) {
                res.json({"status" : true, "message" : "found", "data": events})
            } else {
                res.json({"status" : false, "message" : "empty"})
            }
        }
    )
    .catch(
        err => {
            res.json({"status" : false, "message" : "err", "data": err})
        }
    )
}


exports.filters    = function(req, res){

    function getUniqueValuesOfKey(array, key){
        return array.reduce(function(carry, item){
            if(item[key] && !~carry.indexOf(item[key])) carry.push(item[key])
            return carry
        }, [])
    }

    Event.find({}, "city category")
    .exec()
    .then(
        e => {
            if (e) {

                const city      = getUniqueValuesOfKey(e, 'city')
                const category  = getUniqueValuesOfKey(e, 'category')
                const data      = {
                    'city': city,
                    'category': category
                }

                res.json({"status" : true, "message" : "found", "data": data})
            } else {
                res.json({"status" : false, "message" : "not found"})
            }
        },
        err => {
            res.json({"status" : false, "message" : "error fetching data", "data": err})
        }
    )
}
